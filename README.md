Wagtail Have The Ability To Actually See Fields In The Admin
============================================================

**NOTE:** [wagtailatomicadmin](https://github.com/alexgleason/wagtailatomicadmin) is now likely a better fix for this problem.

Alright... you messed up. Maybe you didn't think your content through well enough. Maybe you really need to meet a deadline and nesting StreamField blocks is the only way through.

Look, I don't judge you. I'm here to help. Just `pip install wagtailhavetheabilitytoactuallyseefieldsintheadmin`, add it to your installed apps, and you're good to go. Just don't say you got it from me, okay?

It'll basically just make nested fields wide enough to see without getting claustrophobic.

For instance, when you've got a StreamField like this:

![Without wagtailhavetheabilitytoactuallyseefieldsintheadmin](without-wagtailhavetheabilitytoactuallyseefieldsintheadmin.png)

We can make it look like this:

![With wagtailhavetheabilitytoactuallyseefieldsintheadmin](with-wagtailhavetheabilitytoactuallyseefieldsintheadmin.png)

Much better. By the way, it basically just expands nested fields to cover the help text placeholders. If you use help text inside of StreamField, you can't use this. It doesn't affect top level fields, though.

But, is there a better way?
---------------------------
Yeah, probably. [It's being discussed](https://github.com/torchbox/wagtail/issues/2305), so hold your horses. This reason this happens in the first place is because some space on the side is saved for help text, which is obviously a good thing to have. So this is gonna take some time to solve long-term if we want to keep that feature.

For complex fields, you should probably just make them look right anyway. See [wagtailblocks_cards](https://github.com/alexgleason/wagtailblocks_cards) for an example of this. If you have time, I suggest giving that a shot. Otherwise, wagtailhavetheabilitytoactuallyseefieldsintheadmin is your friend.

License
-------
This is free and unencumbered software released into the public domain. View the UNLICENSE file in this repository for more details.
